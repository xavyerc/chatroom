﻿using System;
namespace ChatRoom.Data
{
    public class StockFile
    {
        public String Symbol { get; set; }
        public String Date { get; set; }
        public String Time { get; set; }
        public String Open { get; set; }
        public String High { get; set; }
        public String Low { get; set; }
        public String Close { get; set; }
        public String Volume { get; set; }
    }
}

﻿using System;
using System.Net.Http;

namespace ChatRoom.Data
{
    public class HttpClientRequester
    {
        public HttpClient Requester { get; set; }

        public HttpClientRequester(string baseUrl)
        {
            Requester = new HttpClient();
            Requester.BaseAddress = new Uri("https://stooq.com");
            Requester.BaseAddress = new Uri(baseUrl);
            Requester.DefaultRequestHeaders.Accept.Clear();
        }

    }
}

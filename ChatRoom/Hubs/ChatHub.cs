﻿using System;
using System.Threading.Tasks;
using ChatRoom.Models;
using Microsoft.AspNetCore.SignalR;

namespace ChatRoom.Hubs
{
    public class ChatHub : Hub
    {

        public async Task SendMessage(Message message) =>
            await Clients.All.SendAsync("receiveMessage", message);

        public ChatHub()
        {
        }
    }
}

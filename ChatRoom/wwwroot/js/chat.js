﻿class Message {
    constructor(username, text, timestamp) {
        this.userName = username;
        this.text = text;
        this.timestamp = timestamp;
    }
}

const username = userName;
const textInput = document.getElementById('messageText');
const chat = document.getElementById('chat');
const messagesQueue = [];

function clearInputField() {
    messagesQueue.push(textInput.value);
    textInput.value = "";
}

function sendMessage() {
    let text = messagesQueue.shift() || "";
    if (text.trim() === "") return;

    let timestamp = new Date();
    let message = new Message(username, text, timestamp);
    sendMessageToHub(message);
}

function addMessageToChat(message) {
    console.log("Adding message");
    let isCurrentUserMessage = message.userName === username;
    console.log(isCurrentUserMessage);
    let container = document.createElement('div');
    container.className = isCurrentUserMessage ? "container darker" : "container";

    const textAlign = isCurrentUserMessage ? "text-right text-white" : "text-left"
    console.log(textAlign);
    let sender = document.createElement('p');
    sender.className = `sender ${textAlign}`;
    sender.innerHTML = message.userName;
    let text = document.createElement('p');
    text.className = textAlign;
    text.innerHTML = message.text;

    let timestamp = document.createElement('span');
    timestamp.className = isCurrentUserMessage ? "time-right" : "time-left";
    var currentdate = new Date();
    timestamp.innerHTML =
        (currentdate.getMonth() + 1) + "/"
        + currentdate.getDate() + "/"
        + currentdate.getFullYear() + " "
        + currentdate.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })

    container.appendChild(sender);
    container.appendChild(text);
    container.appendChild(timestamp);
    chat.appendChild(container);
}

function addBotMessageToChat(message) {
    console.log("Adding BOT message", message);
    let isCurrentUserMessage = message.userName === username;
    let container = document.createElement('div');
    container.className = isCurrentUserMessage ? "container darker" : "container";

    const textAlign = isCurrentUserMessage ? "text-right text-white" : "text-left"
    console.log(textAlign);
    let sender = document.createElement('p');
    sender.className = `sender ${textAlign}`;
    sender.innerHTML = message.userName;
    let text = document.createElement('p');
    text.className = textAlign;
    text.innerHTML = message.text;

    let timestamp = document.createElement('span');
    timestamp.className = isCurrentUserMessage ? "time-right" : "time-left";
    var currentdate = new Date();
    timestamp.innerHTML =
        (currentdate.getMonth() + 1) + "/"
        + currentdate.getDate() + "/"
        + currentdate.getFullYear() + " "
        + currentdate.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })

    container.appendChild(sender);
    container.appendChild(text);
    container.appendChild(timestamp);
    chat.appendChild(container);
}

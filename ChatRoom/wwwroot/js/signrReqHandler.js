﻿var connection = new signalR.HubConnectionBuilder()
    .withUrl('/Home/Index')
    .build();

connection.logging = true;

connection.on('receiveMessage', addMessageToChat);
connection.on('receiveBotMessage', addBotMessageToChat);

connection.start()
    .catch(error => {
        console.error(error.message);
    });

function sendMessageToHub(message) {
    connection.invoke('sendMessage', message);
}
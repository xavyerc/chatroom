﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
namespace ChatRoom.Models
{
    public class AppUser : IdentityUser
    {

        public virtual ICollection<Message> Messages { get; set; }

        public AppUser()
        {
            Messages = new HashSet<Message>();
        }
    }
}

﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ChatRoom.Models
{
    public class Message
    {
        public int Id { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Text { get; set; }

        public DateTime TimeStamp { get; set; }

        public string UserId { get; set; }

        public virtual AppUser Sender { get; set; }

        public Message()
        {
            TimeStamp = DateTime.Now;
        }
    }
}

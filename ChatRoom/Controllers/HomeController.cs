﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ChatRoom.Models;
using Microsoft.AspNetCore.Identity;
using ChatRoom.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.Net.Http;
using CsvHelper;
using System.IO;
using ChatRoom.Hubs;
using Microsoft.AspNetCore.SignalR;

namespace ChatRoom.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ApplicationDbContext _context;
        private readonly UserManager<AppUser> _userManager;
        private readonly IHubContext<ChatHub> _hubContext;
        static private HttpClientRequester client = new HttpClientRequester("https://stooq.com");

        public HomeController(ILogger<HomeController> logger, IHubContext<ChatHub> hubContext, ApplicationDbContext context, UserManager<AppUser> userManager)
        {
            _logger = logger;
            _context = context;
            _userManager = userManager;
            _hubContext = hubContext;
        }

        public async Task<IActionResult> Index()
        {
            try
            {
                var currentUser = await _userManager.GetUserAsync(User) ?? new AppUser();
                if (User?.Identity?.IsAuthenticated == true)
                {
                    ViewBag.CurrentUserName = currentUser.UserName;
                }
                var messages = await _context.Messages
                    .OrderByDescending(message => message.TimeStamp)
                    .Take(50)
                    .ToListAsync() ?? new List<Message>();
                return View(messages);
            } catch(Exception e)
            {
                return View();
            }
        }

        public async Task<IActionResult> Create(Message message)
        {
            if (ModelState.IsValid)
            {
                message.UserName = User.Identity.Name;
                var sender = await _userManager.GetUserAsync(User);
                message.UserId = sender.Id;
                await _context.Messages.AddAsync(message);
                await _context.SaveChangesAsync();
                if (message.Text.IndexOf('/') == 0 && message.Text.Contains("/stock="))
                {
                    var stockCode = message.Text.Substring(message.Text.IndexOf("=") + 1);
                    var stockMessage = await GetStockAsync(stockCode);
                    try
                    {
                        var botMessage = new Message
                        {
                            Sender = new AppUser { Email = "StockBot" },
                            Text = stockMessage,
                            TimeStamp = new DateTime(),
                            UserName = "StockBot"
                        };
                        await _hubContext.Clients.All.SendAsync("receiveBotMessage", botMessage);
                    }
                    catch (Exception e)
                    {
                        Console.Write(e);
                    }
                }
                return Ok();
            }
            return Error();
        }

        static async Task<string> GetStockAsync(string stockCode)
        {
            var stockMessage = "";
            try
            {
                HttpResponseMessage response = await client.Requester.GetAsync($"/q/l/?e=csv&f=sd2t2ohlcv&h&s={stockCode}");
                if (response.IsSuccessStatusCode)
                {
                    CsvHelper.Configuration.CsvConfiguration config =
                        new CsvHelper.Configuration.CsvConfiguration(new System.Globalization.CultureInfo("en"));
                    config.Delimiter = ",";
                    config.MissingFieldFound = null;
                    var csvFile = await response.Content.ReadAsStreamAsync();
                    List<StockFile> readFile = new List<StockFile>();
                    using (TextReader reader = new StreamReader(csvFile))
                    using (var csv = new CsvReader(reader, config))
                        while (csv.Read())
                        {
                            StockFile stockFile = csv.GetRecord<StockFile>();
                            readFile.Add(stockFile);
                        }
                    if (readFile.FirstOrDefault().Open == "N/D")
                    {
                        stockMessage = $"{readFile.FirstOrDefault().Symbol} is not a valid stock value";
                    }
                    else
                    {
                        stockMessage = $"{readFile.FirstOrDefault().Symbol} quote is ${readFile.FirstOrDefault().Open} per share";
                    }
                }
            }catch(Exception e)
            {
                stockMessage = e.ToString();
            }
            return stockMessage;
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

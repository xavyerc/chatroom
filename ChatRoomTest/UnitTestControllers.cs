using System;
using Xunit;
using ChatRoom.Controllers;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.SignalR;
using ChatRoom.Data;
using Microsoft.AspNetCore.Identity;
using ChatRoom.Models;
using ChatRoom.Hubs;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Microsoft.EntityFrameworkCore;

namespace ChatRoomTest
{
    public class UnitTestControllers
    {

        [Fact]
        public async void TestHomeController()
        {
            var mockLogger = new Mock<ILogger<HomeController>>();
            ILogger<HomeController> _loggerMock = mockLogger.Object;

            DbContextOptions<ApplicationDbContext> options = new DbContextOptions<ApplicationDbContext>();
            var mockDbContext = new Mock<ApplicationDbContext>(options);
            var model = new Mock<Microsoft.EntityFrameworkCore.Metadata.Internal.Model>();
            mockDbContext.Setup(c => c.Model).Returns(model.Object);
            ApplicationDbContext _contextMock = mockDbContext.Object;

            var store = new Mock<IUserStore<AppUser>>();
            var mockUserManager = new Mock<UserManager<AppUser>>(store.Object, null, null, null, null, null, null, null, null);
            UserManager<AppUser> _userManagerMock = mockUserManager.Object;

            var mockHubContext = new Mock<IHubContext<ChatHub>>();
            IHubContext<ChatHub> _hubContextMock = mockHubContext.Object;

            var controller = new HomeController(_loggerMock, _hubContextMock, _contextMock, _userManagerMock);

            var result = await controller.Index();
            var okResult = result as OkObjectResult;

            Assert.NotNull(okResult);
            Assert.Equal(200, okResult.StatusCode);

        }
    }
}
